/* En rust no existe como tal un concepto de interfaces pero tenemos los traits */

// La "interfaz" (trait) animal define dos métodos que deben definir quienes implementen la misma
pub trait Animal {
  fn run(&self) -> &str;
}

// Creamos la "clase" (struct) que se llama dog
struct Dog {}
// Implementamos 
impl Animal for Dog {
  fn run(&self) -> &str{
    return "Dog is running"
  }

}
// Implementamos el método adicional
impl Dog {
  fn bark(&self) -> &str {
    return "Dog is barking"
  }
}

struct Bird {}

impl Animal for Bird {
  fn run(&self) -> &str {
    return "Bird is runing"
  }
}

impl Bird {
  fn fly(&self) -> &str {
    return "Bird is flying"
  }
}

fn main() {
  let firulais = Dog{};
  let piolin = Bird{};

  println!("{}", firulais.run());
  println!("{:}", firulais.bark());

  println!("{}", piolin.run());
  println!("{}", piolin.fly());
}
